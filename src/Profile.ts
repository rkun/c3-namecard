type Profile = {
  name: string,
  avatarURL: string,
  bgURL: string,
  grade: string,
  tags: string[],
  challenges: string,
  hobbies: string,
  comment: string
}

export default Profile